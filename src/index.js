import express from "express"
import server from './server'
const cors = require("cors")
const PORT = 4444
const createServer = async () => {
    try {
        const app = express()
        app.use(cors())
        server.applyMiddleware({
            app
        })
        app.listen({
            port: PORT
        }, () => {
            console.log(`server is starting on port ${PORT}`);
        })

    } catch (error) {
        console.log(error);
    }
}

createServer()