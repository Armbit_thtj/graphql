import {create,upload} from "../../../models/product.model"
const addProduct = async(parent, args, context, info)=>{
    const product = args
    const result = await create(product)
    return result
}

const uploadProducts = async(parent, args, context, info)=>{
    const products = await args.input.map((product,index)=>{
        return Object.values(product)
    })
    const uploaded = await upload(products)
    return uploaded
}

const uploadProductFile = async(parent, args, context, info)=>{
    const file = args.file
    console.log(file);
    const data = {
        info:"hey"
    }
    return data
}

export {
    addProduct,
    uploadProducts,
    uploadProductFile
}