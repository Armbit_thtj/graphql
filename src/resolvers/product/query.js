import {findAllProduct,findById} from "../../../models/product.model"
const findProduct = async (parent, args, context, info)=>{
    const result = await findAllProduct()
    return result
}

const findProductById = async(parent, args, context, info)=>{
    const {id} = args
    const result = await findById({id})
    return result[0]
}

export {
    findProduct,
    findProductById
}