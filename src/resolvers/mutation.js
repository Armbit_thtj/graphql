import {signup,updateUser,addUserAddress} from "./user/mutation"
import {addProduct,uploadProducts} from "./product/mutation"
import {signup_member,login,verify_Token} from "./member/mutation"
const Mutation = {
    signup,
    updateUser,
    addUserAddress,
    addProduct,
    uploadProducts,
    signup_member,
    login,
    verify_Token
    
}

export {
    Mutation
}