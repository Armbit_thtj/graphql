import {
    create,
    findUserById,
    createUserAddress,
    findUserByName,
    update
} from "../../../models/user.model"
const signup = async (parent, args, context, info) => {
    const {
        fname
    } = args
    const hasUser = await findUserByName({
        fname
    })
    if (hasUser.length) throw new Error("fname is exist")
    const result = await create(args)
    return result
}

const updateUser = async (parent, args, context, info) => {
    const user = args
    const {
        id
    } = user
    delete user.id
    const result = await update(user, id)
    return result
}

const addUserAddress = async (parent, args, context, info) => {
    const user_address = args
    const {
        user_id:id
    } = user_address
    const hasUser = await findUserById({id})
    if (!hasUser.length) throw new Error("user not exist")
    const result = await createUserAddress(user_address)
    return result
}
export {
    signup,
    updateUser,
    addUserAddress
}