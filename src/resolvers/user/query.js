import {
    findAll,
    findUserAddressById
} from "../../../models/user.model"
import {
    findOne,
    verifyToken
} from "../../../utils/app"
const findUser = async (parent, args, context, info) => {
    const verified = await verifyToken(context)
    const result = await findAll()
    return result
}

const findUserAddress = async (parent, args, context, info) => {
    const verified = await verifyToken(context)
    const {
        id,
        fname,
        lname,
        address
    } = await findOne(await findUserAddressById(args.id))
    const data = {
        user: {
            id,
            fname,
            lname
        },
        address: {
            address
        }
    }
    return data
}

export {
    findUser,
    findUserAddress
}