import {create,findByUsername} from "../../../models/member.model"
import * as bcrypt from "bcrypt"
import {findOne,genToken,verifyToken} from "../../../utils/app"
const signup_member = async(parent, args, context, info)=>{
    const {member} = args
    const {username,password,confirm_password} = member
    if(password !== confirm_password){
        throw new Error('password and confirm password not equal')
    } 
    const encrypted_password = await bcrypt.hash(password,10)
    const signup_data = {
        username:username,
        password:encrypted_password
    }
    const signedup = await create(signup_data)
    return signedup
}

const login = async(parent, args, context, info)=>{
    const {username,password} = args
    const member = await findOne(await findByUsername({username}))
    const compared = await bcrypt.compare(password,member.password)
    if(!compared) throw new Error("username or password invalid")
    const token = await genToken(member)
    return {username,token}
    
}

const verify_Token = async(parent, args, {req}, info)=>{
    console.log("context ",req.headers);
    const {token} = args
    const verified = await verifyToken(token)
    return verified
}
export{
    signup_member,
    login,
    verify_Token
}