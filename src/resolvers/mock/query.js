import {
    USER
} from "../../../constants/mock"

const me_mock = USER[0]

const me = async (parent, args, context, info) => {
    return me_mock
}

const user = async (parent, args, context, info) => {
    const id = args.id
    const user = USER.find((u) => u.id === id)
    return user
}

const users = async (parent, args, context, info) => {
    return USER
}

const findNested = async (parent, args, context, info) => {
    const data = {
        main: {
            title: "Nested",
            content: "abcdef123456"
        }
    }
    return data
}
export {
    me,
    user,
    users,
    findNested
}