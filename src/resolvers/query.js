import {findUser,findUserAddress} from "./user/query"
import {me,user,users,findNested} from "./mock/query"
import {findProduct,findProductById} from "./product/query"
const Query = {
    me,
    user,
    users,
    findNested,
    findUser,
    findUserAddress,
    findProduct,
    findProductById

}

export {
    Query
}