import {
    ApolloServer,
} from "apollo-server-express"
import path from "path"
import fs from "fs"
import resolvers from "./resolvers/resolvers"
const typeDefs = fs.readFileSync(path.join(__dirname,"./schema","schema.graphql"),'utf8')
const server = new ApolloServer({
    typeDefs,
    resolvers,
    context:({req})=>({req})
})

export default server