import * as jwt from "jsonwebtoken"
import {
    SECRET_KEY
} from "../constants/config"

const findOne = async (data) => {
    return data[0] || []
}

const genToken = async (data) => {
    const token = jwt.sign({
        user: data
    }, SECRET_KEY, {
        expiresIn: "60000ms"
    })
    return token
}

const verifyToken = async (context) => {
    try {
        const bearer_token = context.req.headers.authorization 
        if(!bearer_token){
            // return {status:false,message:"Invalid Token"}
            throw new Error("Invalid Token")
        } 
        const token = bearer_token.split(" ")[1]
        const verified = await jwt.verify(token, SECRET_KEY)
        return {status:true,message:"OK"}
    } catch (error) {
        throw new Error(error.message)
    }
}
export {
    findOne,
    genToken,
    verifyToken
}