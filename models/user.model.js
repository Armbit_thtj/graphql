import db from "../env/db_connection"

const findAll = async()=>{
    const result = await db.query(`SELECT * FROM users`)
    return result
}

const findUserById = async({id})=>{
    const result = await db.query(`SELECT * FROM users WHERE id = ?`,[id])
    return result
}

const findUserAddressById = async(id)=>{
    const result = await db.query(`SELECT users.id,users.fname,users.lname,user_address.address FROM user_address LEFT JOIN users  ON user_address.user_id = users.id WHERE users.id = ?`, [id])
    return result
}

const findUserByName = async({fname})=>{
    const result = await db.query(`SELECT * FROM users WHERE fname=?`,[fname])
    return result
}

const create = async(user)=>{
    const result = await db.query(`INSERT INTO users SET ?`, [user])
    return result
}

const createUserAddress = async(user_address)=>{
    const result = await db.query(`INSERT INTO user_address SET ?`,[user_address])
    return result
}

const update = async(user,id)=>{
    const result = await db.query(`UPDATE users SET ? WHERE id = ?`,[user,id])
    return result
}
    
export {
    findAll,
    findUserById,
    findUserAddressById,
    findUserByName,
    create,
    createUserAddress,
    update
}