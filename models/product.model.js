import db from "../env/db_connection"
const findAllProduct = async()=>{
    const result = await db.query(`SELECT * FROM products`)
    return result
}

const findById = async({id})=>{
    const result = await db.query(`SELECT * FROM products WHERE id = ?`,[id])
    return result
}

const create = async(product)=>{
     const result = await db.query(`INSERT INTO products SET ?`,[product])
     return result
}

const upload = async(products)=>{
    const result = await db.query(`INSERT INTO products(name,price,amount) VALUES ?`,[products])
    return result
}

export {
    findAllProduct,
    findById,
    create,
    upload
}