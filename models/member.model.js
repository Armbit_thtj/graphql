import db from "../env/db_connection"

const create = async(member)=>{
    const result = await db.query(`INSERT INTO members SET ?`,[member])
    return result
}

const findByUsername = async({username})=>{
    const result = await db.query(`SELECT username,password FROM members WHERE username =?`,[username])
    return result
}


export {
    create,
    findByUsername
}