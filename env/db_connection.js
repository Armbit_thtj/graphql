import * as mysql from "mysql"
import { promisify } from "util"
import {MYSQL} from "./development"

const dbConfig = MYSQL
const pool = mysql.createPool(dbConfig)
const query = promisify(pool.query).bind(pool)

export default {
    query
}