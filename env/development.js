const MYSQL = {
    queueLimit: 0, // unlimited queueing
    connectionLimit: 120, // unlimited connections
    multipleStatements: true,
    host: "localhost",
    port: 3306,
    user: "root",
    password: "123456789",
    database: "graph_ql",
    debug: false,
  }

  export{
      MYSQL
  }